<?php
    $full_desc = get_field( 'full_desc' );
    $screenshot = get_field( 'screenshot' );
    $url = get_field( 'url' );
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
	    <div class="content">
			<div class="container">
				<?php get_template_part( 'sidebar' ); ?>
				<main class="page-content">
					<div class="portfolio">
						<h1 class="title title_single"><?php the_title(); ?></h1>
						<div class="single__portfolio">

                            <?php if ( $screenshot ) : ?>
                                <div class="single__image">
                                    <a href="#" class="single__image-link">
                                        <img src="<?php echo $screenshot['url']; ?>" alt="" class="single__image-pic">
                                    </a>
                                </div>
                            <?php endif; ?>

							<div class="single__desc">
                                <?php if ( $full_desc ) : ?>
                                    <?php echo $full_desc; ?>
                                <?php endif; ?>
							</div>
                            <?php if ( $url ) : ?>
                                <div class="about__bttns single__bttns">
                                    <a href="<?php echo $url; ?>" class="btn" target="_blank">
                                       <i class="fa fa-arrows-alt" aria-hidden="true"></i>
                                        <span class="btn-text">В полном размере</span>
                                    </a>
                                </div>
                            <?php endif; ?>

						</div>
					</div>
				</main><!-- page-content -->
			</div><!-- container -->
		</div>
    <?php endwhile; ?>
<?php endif; wp_reset_query(); ?>

<?php get_footer(); ?>
