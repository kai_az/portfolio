<?php
/*
* Template name: Портфолио
*/
?>
<?php get_header(); ?>

<div class="content">
	<div class="container">
		<?php get_template_part( 'sidebar' ); ?>
		<main class="page-content">
			<div class="portfolio">
				<h1 class="title"><?php the_title(); ?></h1>

				<?php
					$args = array(
						'post_type' 	 => 'portfolio_item',
						'posts_per_page' => -1
					);

					$wp_query = new WP_Query( $args );
				?>
				<?php if ( $wp_query->have_posts() ) : ?>
					<ul class="portfolio__list">
					    <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
							<?php
								$terms = wp_get_post_terms( get_the_ID(), 'portfolio_cat' );
								$desc = get_field( 'desc' );
								$full_desc = get_field( 'full_desc' );
								$screenshot = get_field( 'screenshot' );
								$url = get_field( 'url' );
								$screenshot = $screenshot['sizes']['portfolio-thumb'];
							?>
							<li class="portfolio__item">
								<a href="<?php the_permalink(); ?>" class="portfolio__item-link">
									<div class="portfolio__img">
										<?php if ( $screenshot ) : ?>
											<img src="<?php echo $screenshot; ?>" alt="" class="portfolio__img-pic">
										<?php endif; ?>
									</div>
									<div class="portfolio__text">
										<div class="portfolio__text-title"><?php the_title(); ?></div>

										<?php if ( $desc ) : ?>
											<div class="portfolio__title-desc"><?php echo $desc; ?></div>
										<?php endif; ?>

										<?php if ( isset( $terms ) && ! empty( $terms ) ) : ?>
											<?php foreach ( $terms as $term ) : ?>
												<?php $icon_class = get_field( 'icon_class', 'portfolio_cat_' . $term->term_id ); ?>
												<div class="portfolio__device">

													<?php if ( $icon_class ) : ?>
														<i class="fa <?php echo $icon_class; ?>" aria-hidden="true"></i>
													<?php else : ?>
														<i class="fa fa-desktop" aria-hidden="true"></i>
													<?php endif; ?>

													<div class="portfolio__device-text"><?php echo $term->name; ?></div>
												</div>
											<?php endforeach; ?>
										<?php endif; ?>

									</div>
								</a>
							</li>

					    <?php endwhile; ?>
					</ul>
				<?php endif; wp_reset_postdata(); ?>
			</div>
		</main><!-- page-content -->
	</div><!-- container -->
</div>


<?php get_footer(); ?>
