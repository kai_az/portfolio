<?php
/*
* Template name: Обо мне
*/
?>
<?php get_header(); ?>

<div class="content">
    <div class="container">
            <?php get_template_part( 'sidebar' ); ?>
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php
                        $photo = get_field( 'photo' );
                        $resume_download = get_field( 'resume_download' );
                    ?>
                    <main class="page-content">
                        <div class="about">

                            <div class="about__left">
                                <div class="about__pic">
                                    <?php if ( $photo ) : ?>
                                        <img src="<?php echo $photo['url']; ?>" alt="" class="about__pic-img">
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="about__right">
                                <h1 class="title"><?php the_title(); ?></h1>
                                <div class="about__desc">
                                    <?php the_content(); ?>
                                </div>
                                <div class="about__bttns">
                                    <?php if ( $resume_download ) : ?>
                                        <a href="<?php echo $resume_download; ?>" class="btn" target="_blank">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            <span class="btn-text">Скачать резюме</span>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div><!-- about__right -->

                        </div><!-- about -->
                    </main><!-- page-content -->
                <?php endwhile; ?>
            <?php endif; wp_reset_query(); ?>
    </div><!-- container -->
</div>

<?php get_footer(); ?>
