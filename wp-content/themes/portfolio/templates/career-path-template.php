<?php
/*
* Template name: Карьерный путь
*/
?>
<?php get_header(); ?>

<div class="content">
    <div class="container">
            <?php get_template_part( 'sidebar' ); ?>
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php $image = get_field('image'); ?>
                    <main class="page-content">
                        <div class="career">
                            <div class="career__left">
                                <?php if ( $image ) : ?>
                                    <div class="career__pic">
                                        <img src="<?php echo $image['sizes']['medium']; ?>" alt="" class="career__pic-img">
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="career__right">
                                <ul class="career__list">
                                    <?php if( have_rows( 'path' ) ) : ?>
                                            <?php while ( have_rows( 'path' ) ) : the_row(); ?>
                                                <?php
                                                    $date = get_sub_field( 'date' );
                                                    $desc = get_sub_field( 'desc' );
                                                ?>

                                                <li class="career__item">
                                                    <div class="career__block">
                                                        <?php if ( $desc ) : ?>
                                                            <div><?php echo $desc; ?></div>
                                                        <?php endif; ?>
                                                        <?php if ( $date ) : ?>
                                                            <div class="career__year"><?php echo $date; ?></div>
                                                        <?php endif; ?>
                                                    </div>
                                                </li>
                                            <?php endwhile; ?>
                                    <?php endif; ?>
                                </ul>
                            </div><!-- about__right -->
                        </div><!-- about -->
                    </main><!-- page-content -->

                <?php endwhile; ?>
            <?php endif; wp_reset_query(); ?>
    </div><!-- container -->
</div>

<?php get_footer(); ?>
