<aside class="sidebar">
    <?php
        wp_nav_menu( array(
            'theme_location' => 'sidebar-menu',
            'menu_class'     => 'nav__list',
            'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
            'walker'         => new WP_Bootstrap_Navwalker()
        ));
    ?>
</aside><!-- sidebar -->
