<?php
    $copyright = get_field( 'copyright', 'option' );
?>

    </div>
</div>
<footer class="footer">
    <div class="container">

        <div class="footer__left">
            <?php if ( $copyright ) : ?>
                <div class="copy">
                    <?php echo $copyright; ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="footer__right">

            <?php if( have_rows( 'social_icons', 'option' ) ) : ?>
                <div class="socials">
                    <?php while ( have_rows( 'social_icons', 'option' ) ) : the_row(); ?>
                        <?php
                            $url = get_sub_field( 'url' );
                            $icon = get_sub_field( 'icon' );
                        ?>
                        <div class="socials__item">
                            <a href="<?php echo $url; ?>" class="social__link" target="_blank"><img src="<?php echo $icon['sizes']['thumbnail']; ?>" alt="" /></a>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>



    </div><!-- container -->
    <?php wp_footer(); ?>
</footer>


</body>
</html>
