<?php

// Register Custom Navigation Walker
require_once( get_template_directory() . '/includes/wp-bootstrap-navwalker.php' );


add_filter( 'wpcf7_validate_configuration', '__return_false' );

// Debug tool
if ( ! function_exists( 'pr' ) ) {
    function pr( $val ) {
        echo '<pre class="wpm-debug">';
        print_r( $val );
        echo '</pre>';
    }
}

if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'portfolio-thumb', 220, 172, true );
}

function sws_add_assets_portfolio() {
    /**
	* Include styles
	*/
    wp_enqueue_style( 'main-css', get_template_directory_uri() . '/assets/css/main.css?time=' . time() );
	wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/style.css?time=' . time() );

    /**
	* Include scripts
	*/
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ) );
	// wp_enqueue_script( 'match-height-plugin', get_template_directory_uri() . '/assets/js/jquery.matchHeight-min.js', array( 'jquery' ) );

    wp_localize_script( 'main-js', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
}
add_action( 'wp_enqueue_scripts', 'sws_add_assets_portfolio' );

function sws_register_my_portfolio_menu() {
    register_nav_menu( 'sidebar-menu', __( 'Sidebar Menu' ) );
}
add_action( 'init', 'sws_register_my_portfolio_menu' );

/**
 * Register a portfolio post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */

$labels = array(
     'name'               => __( 'Portfolio', 'portfolio' ),
     'singular_name'      => __( 'Portfolio', 'portfolio' ),
     'menu_name'          => __( 'Portfolio', 'portfolio' ),
     'name_admin_bar'     => __( 'Portfolio', 'portfolio' ),
     'add_new'            => __( 'Add New Portfolio work', 'portfolio' ),
     'add_new_item'       => __( 'Add New Portfolio work', 'portfolio' ),
     'new_item'           => __( 'New Portfolio work', 'portfolio' ),
     'edit_item'          => __( 'Edit Portfolio work', 'portfolio' ),
     'view_item'          => __( 'View Portfolio work', 'portfolio' ),
     'all_items'          => __( 'All works in portfolio', 'portfolio' ),
     'search_items'       => __( 'Search Portfolio works', 'portfolio' ),
     'parent_item_colon'  => __( 'Parent Portfolio works:', 'portfolio' ),
     'not_found'          => __( 'No found.', 'portfolio' ),
     'not_found_in_trash' => __( 'No Portfolio works found in Trash.', 'portfolio' )
);

$args = array(
     'labels'             => $labels,
     'description'        => __( 'Portfolio', 'portfolio' ),
     'public'             => true,
     'publicly_queryable' => true,
     'show_ui'            => true,
     'show_in_menu'       => true,
     'query_var'          => 'portfolio',
     'rewrite'            => true,
     'capability_type'    => 'post',
     'has_archive'        => true,
     'hierarchical'       => false,
     'menu_position'      => null,
     'menu_icon'          => 'dashicons-art',
     'supports'           => array( 'title', 'custom-fields', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
);

register_post_type( 'portfolio_item', $args );

// Add new taxonomy, make it hierarchical (like categories)
$labels = array(
	'name'              => _x( 'Portfolio category', 'taxonomy general name' ),
	'singular_name'     => _x( 'Portfolio category', 'taxonomy singular name' ),
	'search_items'      => __( 'Search Portfolio Category' ),
	'all_items'         => __( 'All Portfolio Categories' ),
	'popular_items'     => __( 'Popular Categories' ),
	'parent_item'       => __( 'Parent Portfolio Category' ),
	'parent_item_colon' => __( 'Parent Portfolio Category:' ),
	'edit_item'         => __( 'Edit Portfolio Category' ),
	'update_item'       => __( 'Update Portfolio Category' ),
	'add_new_item'      => __( 'Add New Portfolio Category' ),
	'new_item_name'     => __( 'New Portfolio Category Name' ),
	'menu_name'         => __( 'Portfolio categories' ),
);

$args = array(
	'hierarchical'      => true,
	'labels'            => $labels,
	'show_ui'           => true,
	'show_admin_column' => true,
	'query_var'         => true,
	'rewrite'           => array( 'slug' => 'portfolio-category' ),
);

register_taxonomy( 'portfolio_cat', array( 'portfolio_item' ), $args );

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}
