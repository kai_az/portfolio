<?php get_header(); ?>

<div class="content">
    <div class="container">
            <?php get_template_part( 'sidebar' ); ?>
            <main class="page-content">
                <div class="portfolio">
                    <?php if ( have_posts() ) : ?>
                        <?php while ( have_posts() ) : the_post(); ?>
                    	    <h1 class="title"><?php the_title(); ?></h1>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php endif; wp_reset_query(); ?>
                </div>
            </main><!-- page-content -->
    </div><!-- container -->
</div>


<?php get_footer(); ?>
