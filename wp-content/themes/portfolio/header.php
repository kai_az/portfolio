<?php
	$name = get_field( 'field_5962170153e6e', 'option' );
	$photo = get_field( 'photo', 'option' );
?>
<!DOCTYPE html>
<html lang="ru-Ru">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
		<?php bloginfo( 'name' ); ?>
		<?php if ( is_front_page() ) :?>
			<?php echo bloginfo( 'description' ); ?>
		<?php else : ?>
			<?php echo ' | '; wp_title( '' ); ?>
		<?php endif; ?>
	</title>

    <?php wp_head(); ?>
</head>
<body>
	<div class="wrapper">
		<div class="main-content">
			<header class="header">
				<div class="container">
					<div class="header__right">
						<div class="user">
							<?php if ( $photo ) : ?>
								<div class="user__avatar"><a href="" class="avatar__link">
								<img src="<?php echo $photo['sizes']['thumbnail']; ?>" alt=""></a></div>
							<?php endif; ?>

							<?php if ( $name ) : ?>
								<div class="user__username"><?php echo $name; ?></div>
							<?php endif; ?>

						</div>
					</div><!-- header__right -->
					<div class="header__left">
						<?php
							global $post;
							$thePostID=$post->ID;
							if ( $thePostID == 49 ) {
								$active_class = 'curent--active';
							}
						?>
						<div class="contacts">
							<a href="<?php bloginfo( 'url' ); ?>/contacts/" class="contacts__link <?php echo $active_class; ?>">Мои контакты</a>
						</div>

						<ul class="contacts__dropdown">
							<li class="contacts__dropdown-item"><a href="#"></a></li>
							<li class="contacts__dropdown-item"><a href="#"></a></li>
							<li class="contacts__dropdown-item"><a href="#"></a></li>
						</ul>
					</div><!-- header__left -->
				</div><!-- container -->
			</header>
